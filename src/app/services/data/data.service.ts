import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { allPokeData } from 'src/app/interfaces/data.interface';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root',
})
export class DataService implements allPokeData {
  public env: string;
  public name: string;
  public url: string;
  public arr = [];
  constructor(private http: HttpClient) {
    this.env = environment.url;
  }
  getAllData(): Observable<allPokeData[]> {
    return this.http.get<allPokeData[]>(this.env + 'pokemon?limit=20');
  }
  getById(name: string): Observable<any> {
    return this.http.get(this.env + `${'pokemon/' + name}`);
  }
  async addFav(items) {
    await this.arr.push({ items });
  }
  async showFav() {
    return await this.arr;
  }
}
