import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage implements OnInit {
  public show: boolean = true;
  public hide: boolean = false;
  constructor(private router: Router) { }

  ngOnInit() {
  }
  async goToHome() {
    this.show = false;
    this.hide = true;
   await setTimeout(() => {
      this.router.navigateByUrl('/home')
    }, 10000);
  }
}
