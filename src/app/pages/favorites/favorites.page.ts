import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data/data.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.page.html',
  styleUrls: ['./favorites.page.scss'],
})
export class FavoritesPage implements OnInit {
  public received: string;
  public data: any = {};
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private serviceData: DataService
  ) {
    this.received = this.route.snapshot.paramMap.get('items');
  }

  ngOnInit() {
    this.showDataFav();
  }
  showDataFav() {
    try {
      this.data = this.serviceData.arr;
      return this.data;
    } catch (error) {
      console.log(error);
    }
  }
  search(evt) {
    try {
      const event = evt.srcElement.value;
      if (!event) {
        return;
      }
      this.data = this.data.filter((item) => {
        if (item.items.name && event) {
          return (
            item.items.name.toLowerCase().indexOf(event.toLowerCase()) > -1
          );
        }
      });
    } catch (error) {
      console.error(error);
    }
  }
  goToHome() {
    this.router.navigateByUrl('home');
  }
  goToDetails(items) {
    this.router.navigate(['detail/', items.name]);
  }
}
