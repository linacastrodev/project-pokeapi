import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { DataService } from 'src/app/services/data/data.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
  public received: string;
  public data = {};
  constructor(
    private route: ActivatedRoute,
    private serviceData: DataService,
    private router: Router,
    private toastController:ToastController
  ) {}
  ngOnInit() {
    this.received = this.route.snapshot.paramMap.get('name');
    this.getById();
    this.presentToast();
  }
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'In this page you can see the detail of the pokemon :D',
      duration: 2000,
      position: 'bottom',
      mode:"ios"
    });
    toast.present();
  }
  async getById() {
    try {
      await this.serviceData.getById(this.received).subscribe((response) => {
        this.data = response;
        console.log(this.data)
      });
    } catch (error) {
      console.error(error);
    }
  }
  goFav(items) {
    this.router.navigate(['fav']);
    this.serviceData.addFav(items);
    this.serviceData.showFav();
  }
}
