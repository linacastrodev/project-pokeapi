import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, ToastController } from '@ionic/angular';
import { DataService } from 'src/app/services/data/data.service';
import { DetailsPage } from '../details/details.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  public data: any = {};
  constructor(
    private serviceData: DataService,
    private router: Router,
    private toastController: ToastController
  ) {}

  ngOnInit() {
    this.allData();
    this.presentToast()
  }
  async allData() {
    try {
      await this.serviceData.getAllData().subscribe((response) => {
        this.data = response;
      });
    } catch (error) {
      console.error(error);
    }
  }
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Welcome to Pokedex By Pokemon!',
      duration: 5000,
      position:"bottom",
      mode:"ios"
    });
    toast.present();
  }
  goToDetails(items) {
    this.router.navigate(['detail/', items.name]);
  }
  search(evt) {
    try {
      const event = evt.srcElement.value;
      if (!event) {
        return;
      }
      this.data.results = this.data.results.filter((item) => {
        if (item.name && event) {
          return item.name.toLowerCase().indexOf(event.toLowerCase()) > -1;
        }
      });
    } catch (error) {
      console.error(error);
    }
  }
  async goFav(items: any) {
    this.router.navigate(['fav']);
    this.serviceData.addFav(items);
    this.serviceData.showFav();
    items.selected = true;
  }
}
